// Console I/O
// Paul Haller

#include <iostream>
#include <conio.h>

void LoopQuote(int &amount);

int main()
{
	int input;

	std::cout << "Input an integer between 1 and 5\n";

	std::cin >> input;

	LoopQuote(input);

	_getch();
	return 0;
}

void LoopQuote(int &amount)
{
	if (amount >= 1 && amount <= 5)
	{
		for (int i = 0; i < amount; i++)
		{
			std:: cout << "\"Those who dare to fail miserably can achieve greatly.\"\t - John F.Kennedy\n";
		}
	}
	else
	{
		std::cout << "Error: Integer entered not between 1 and 5";
	}
}